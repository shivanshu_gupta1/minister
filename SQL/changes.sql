ALTER TABLE `about` ADD `lang` TEXT NOT NULL AFTER `description`;
ALTER TABLE `bio` ADD `lang` TEXT NOT NULL AFTER `description`;
ALTER TABLE `literary` ADD `lang` TEXT NOT NULL AFTER `description`;

UPDATE `literary` SET `lang`='English';
UPDATE `bio` SET `lang`='English';