<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Dr. Ramesh Pokhriyal 'Nishank'</title>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="css/fullpage.css" />
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/venobox.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--[if IE]>
        <script type="text/javascript">
             var console = { log: function() {} };
        </script>
    <![endif]-->
    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0&appId=461708817685665&autoLogAppEvents=1" nonce="KLkZlDNx"></script>

</head>
<body>

<div class="innerDivNew">
    <!--*************************header**************-->
  <span id="brugrMenu">
  <button class="hamburger">&#9776;</button>
  <button class="cross">&#735;</button>
    </span>
    <div class="menu">
           <div class="sectionThirteen headerMenuTop">
     <div class="container">
         <div class="col-md-12 col-xs-12 ftrMainLnk">
         
             <div class="col-md-3 col-xs-12 ftrMainLnkOne">
                 <div class="ftrMainLnkOneIn">
                    <h2 class="sabtHdng">About </h2>
                     <ul>
                         <li><a href="index.php">Biography</a></li>
                         <li><a href="index.php">Literary Life</a></li>
                         <li><a href="index.php">Political Life</a></li>
                         <li><a href="news.php">News Updates</a></li>
                     </ul>
                 </div>
             </div>
             <div class="col-md-3 col-xs-12 ftrMainLnkOne">
                 <div class="ftrMainLnkOneIn">
                    <h2 class="sabtHdng">Tune In </h2>
                     <ul>
                     <!-- <li><a href="#11thpage">Watch Live</a></li> -->
                         <li><a href="videos.php">Video Gallery</a></li>
                         <li><a href="photos.php">Photo Gallery</a></li>
                         <li><a href="news.php">Events</a></li>
                     </ul>
                 </div>
             </div>
              <div class="col-md-3 col-xs-12 ftrMainLnkOne">
                 <div class="ftrMainLnkOneIn">
                    <h2 class="sabtHdng">Blog</h2>
                     <ul>
                         <li><a href="videospeeches.php">Speeches</a></li>
                         <li><a href="speeches.php">Text Speeches</a></li>
                         <li><a href="interview.php">Interviews</a></li>
                     </ul>
                 </div>
             </div>
              <div class="col-md-3 col-xs-12 ftrMainLnkOne">
                 <div class="ftrMainLnkOneIn">
                    <h2 class="sabtHdng">Library</h2>
                     <ul>
                         <li><a href="awards.php">Awards</a></li>
                         <li><a href="#11thpage">Poems</a></li>
                         <li><a href="books.php">Books</a></li>
                     </ul>
                 </div>
             </div>
             
         </div>
         <div class="col-xs-12 menuSocialDiv_mn">
                 <div class="menuSocialDiv">
        
                <ul>
                    <li><a href="https://www.facebook.com/cmnishank/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</a></li>
                    <li><a href="https://twitter.com/DrRPNishank" target="_blank"> <i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</a></li>
                    <li><a href="https://www.instagram.com/rameshpokhriyal_official/" target="_blank"> <i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                </ul>
           
    </div>
             </div>
     </div>
   </div>
    
        </div>
        
        <div class="headerNewDivNew">
            <div class="mantriJiNameMobile"><h2>Ramesh Pokhriyal ‘Nishank’<!--  <span>Ministry of Human Resource Development</span> --></h2></div>
        <div class="topOpenNavDiv">
        <ul class="translation-links">
         <li><a href="#4thpage">About</a></li>
         <li><a href="#7thpage">News</a></li>
         <li><a href="#6thpage">Awards</a></li>
         <li><a href="#3rdPage">Governance</a></li>
         <!-- <li><a href="javascript:void(0)">Blog</a></li> -->
         <li><a href="#5thpage">Poet & Author</a></li>
         <li><a href="#8thpage">Connect</a></li>
         <li><a href="net.php">NEP</a></li>
         <!-- <li><a href="#" class="hindi" data-lang="Hindi">Hindi</a></li> -->
     </ul>
   </div>
   <div class="container centerTextDiv">
                <div class="row align-items-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <h3>Dr. Ramesh Pokhriyal 'Nishank'</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sectionInnerDiv">
          <div class="container-fluid">

              <div class="_mainBooksDiv newBooksDiv">
      <div class="row">
      
      <?php
            include './src/dbh.php';
            $sql = "SELECT * FROM news ORDER BY date(date)DESC;";
            $result = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
              $description = substr($row['description'],0,50);
              echo '
              <div class="col-md-3 col-sm-4 col-xs-12 listOne">
                <div class="listOneIn">
                  <br />
                  <br />
                  <img src="./src/'.$row['image'].'">
                  <div class="awardsOverCnt_new"><p>'.$row['title'].'</p><a href="news-old.php?id='.$row['id'].'" class="_buyBtn_new">View Now</a></div>
                </div>
              </div> 
              ';                          
          }
      ?>
      </div>
    </div>
            </div>
          </div>


     </div>
     
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/venobox.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'hi', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
        }
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<script type="text/javascript">
    $('.translation-links a').click(function() {
      var lang = $(this).data('lang');
      var $frame = $('.goog-te-menu-frame:first');
      if (!$frame.size()) {
        alert("Error: Could not find Google translate frame.");
        return false;
      }
      $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
      return false;
    });
</script>


</body>
</html>